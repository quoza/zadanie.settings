package zadanie;

public class GraphicsCard {
    private GraphicsSetting graphicsSetting;

    public GraphicsCard(GraphicsSetting graphicsSetting) {
        this.graphicsSetting = graphicsSetting;
    }

    public GraphicsSetting getGraphicsSetting() {
        return graphicsSetting;
    }

    public void setGraphicsSetting(GraphicsSetting graphicsSettingNew) {
        this.graphicsSetting = graphicsSettingNew;
    }

    public long getPowerFromSettting(){
        return graphicsSetting.getNeededProcessingPower();
    }

    public void printTable(int[][] tablicaIntow){
        graphicsSetting.processFrame(tablicaIntow);
    }
}
