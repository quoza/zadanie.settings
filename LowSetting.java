package zadanie;

public class LowSetting implements GraphicsSetting {

    public long getNeededProcessingPower(){
        return 100;
    }

    public void processFrame(int[][] tablica){
        for (int i = 0; i < tablica.length; i++){
            for(int j = 0; j < tablica[i].length; j++){
                tablica[i][j] = 'L';
            }
        }
    }
}
