package zadanie;

public interface GraphicsSetting {
    public long getNeededProcessingPower();
    public void processFrame(int[][] tablica);
}
